package io.codex.service.inventoryservice.business.domain.model;

import io.codex.service.core.context.RequestContextHolder;
import io.codex.service.core.dao.entities.base.BaseEntity;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity(name = "product_inventory_line_item")
public class ProductInventoryLineItem extends BaseEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "sku_code", nullable = false)
    private String skuCode;

    @Column(name= "sku_identifier", nullable = false)
    private String skuIdentifier;

    @Column(name = "is_reserved", nullable = false, columnDefinition = "TINYINT(1)")
    private Boolean isReserved = false;

    @Column(name = "correlation_id")
    private String correlationId;

    @PreUpdate
    public void setCorrelationId(){
        this.correlationId = RequestContextHolder.getCorrelationId();
    }
    @Builder
    public ProductInventoryLineItem(@NotNull LocalDateTime createdDateTime, LocalDateTime updatedDateTime, Long id, String skuCode, String skuIdentifier, Boolean isReserved) {
        super(createdDateTime, updatedDateTime);
        this.id = id;
        this.skuCode = skuCode;
        this.skuIdentifier = skuIdentifier;
        this.isReserved = isReserved;
    }
}
