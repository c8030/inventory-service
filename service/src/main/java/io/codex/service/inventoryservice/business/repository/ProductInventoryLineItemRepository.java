package io.codex.service.inventoryservice.business.repository;

import io.codex.service.inventoryservice.business.domain.model.ProductInventoryLineItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ProductInventoryLineItemRepository extends JpaRepository<ProductInventoryLineItem, Long> {

    Optional<ProductInventoryLineItem> findFirstBySkuCodeAndIsReservedFalse(String skuCode);

    List<ProductInventoryLineItem> findByCorrelationId(String correlationId);
}