package io.codex.service.inventoryservice.facade.messaging;

import io.codex.service.core.exceptions.base.BaseUncheckedException;
import io.codex.service.inventoryservice.business.manager.InventoryManager;
import io.codex.service.inventoryservice.dto.request.ReserveInventoryRequestDTO;
import io.codex.service.orderservice.dto.OrderDTO;
import io.codex.service.orderservice.dto.OrderLineItemDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
@ExternalTaskSubscription("COMMAND.RESERVE_INVENTORY")
public class ReserveInventoryCommandHandler implements ExternalTaskHandler {

    private final InventoryManager inventoryManager;

    @Override
    public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) {
        log.info("executing reserve inventory command");
        OrderDTO orderDTO = externalTask.getVariable("payload");

        try{
            inventoryManager.reserveInventory(prepareInventoryReserveRequest(orderDTO.getOrderLineItems()));
            externalTaskService.complete(externalTask);
        } catch (BaseUncheckedException ex) {
            log.error(ex.getMessage(), ex);
            externalTaskService.handleBpmnError(externalTask, "INVENTORY.RESERVATION.FAILED", ex.getMessage());
        }
    }

    private List<ReserveInventoryRequestDTO> prepareInventoryReserveRequest(List<OrderLineItemDTO> orderLineItems){
        List<ReserveInventoryRequestDTO> inventoryRequestDTOS = new ArrayList<>();

        orderLineItems.forEach(currItem -> inventoryRequestDTOS.add(prepareInventoryReserveRequest(currItem)));

        return inventoryRequestDTOS;
    }

    private ReserveInventoryRequestDTO prepareInventoryReserveRequest(OrderLineItemDTO orderLineItem){
        return ReserveInventoryRequestDTO.builder()
                .skuCode(orderLineItem.getSkuCode())
                .quantity(orderLineItem.getQuantity())
                .build();
    }
}
