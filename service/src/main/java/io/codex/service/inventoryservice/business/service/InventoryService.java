package io.codex.service.inventoryservice.business.service;

import io.codex.service.inventoryservice.dto.ProductInventoryLineItemDTO;
import io.codex.service.inventoryservice.dto.request.ReserveInventoryRequestDTO;

import java.util.List;

public interface InventoryService {

    void reserveInventory(final ReserveInventoryRequestDTO reserveInventoryRequestDTO);

    List<ProductInventoryLineItemDTO> findInventoryLineItemsByCorrelationId(final String correlationId);

    void saveInventories(List<ProductInventoryLineItemDTO> inventoryLineItemDTOS);

}
