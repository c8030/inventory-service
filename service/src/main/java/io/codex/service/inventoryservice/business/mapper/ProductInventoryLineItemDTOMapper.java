package io.codex.service.inventoryservice.business.mapper;

import io.codex.service.inventoryservice.business.domain.model.ProductInventoryLineItem;
import io.codex.service.inventoryservice.dto.ProductInventoryLineItemDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ProductInventoryLineItemDTOMapper {
    
    ProductInventoryLineItemDTO toDTO(ProductInventoryLineItem productInventoryLineItem);

    List<ProductInventoryLineItemDTO> toDTOs(List<ProductInventoryLineItem> productInventories);

    ProductInventoryLineItem toEntity(ProductInventoryLineItemDTO productInventoryLineItemDTO);

    List<ProductInventoryLineItem> toEntities(List<ProductInventoryLineItemDTO> productInventoryLineItemDTOs);

}
