package io.codex.service.inventoryservice.business.manager;

import io.codex.service.inventoryservice.dto.request.ReserveInventoryRequestDTO;

import java.util.List;

public interface InventoryManager {

    void reserveInventory(List<ReserveInventoryRequestDTO> reserveInventoryRequestDTOs);

    void releaseInventory(final String correlationId);

}
