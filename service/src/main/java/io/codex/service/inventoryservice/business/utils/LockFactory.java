package io.codex.service.inventoryservice.business.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.WeakHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
public class LockFactory {

    private static final Integer maxLocks = 100;

    private static WeakHashMap<Integer, Lock>  lockMap = new WeakHashMap<>(maxLocks);

    public static Lock getLock(final String skuCode){
        log.trace("Getting lock for: {}", skuCode);

        Integer hash = getHash(skuCode);
        lockMap.putIfAbsent(hash, new ReentrantLock(true));

        return lockMap.get(hash);
    }

    private static Integer getHash(final String str){
        return Integer.valueOf(str.hashCode() % maxLocks);
    }

}
