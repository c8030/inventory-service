package io.codex.service.inventoryservice.business.exception;

import io.codex.service.core.exceptions.base.NotFoundException;
import io.codex.service.core.exceptions.base.UnknownException;

public class ServerConflictException extends UnknownException {

    public ServerConflictException(String message, Throwable cause) {
        super(message, cause);
    }

    public ServerConflictException(String message) {
        super(message);
    }
}
