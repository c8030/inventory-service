package io.codex.service.inventoryservice.business.manager.impl;

import io.codex.service.inventoryservice.business.exception.ServerConflictException;
import io.codex.service.inventoryservice.business.manager.InventoryManager;
import io.codex.service.inventoryservice.business.service.InventoryService;
import io.codex.service.inventoryservice.business.utils.LockFactory;
import io.codex.service.inventoryservice.dto.ProductInventoryLineItemDTO;
import io.codex.service.inventoryservice.dto.request.ReserveInventoryRequestDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;

@Slf4j
@Component
@RequiredArgsConstructor
public class InventoryManagerImpl implements InventoryManager {

    private final InventoryService inventoryService;

    @Override
    @Transactional
    public void reserveInventory(List<ReserveInventoryRequestDTO> reserveInventoryRequestDTOs) {
        reserveInventoryRequestDTOs.forEach(currRequest -> reserveInventoryAtomically(currRequest));
    }

    @Override
    @Transactional
    public void releaseInventory(final String correlationId) {
        List<ProductInventoryLineItemDTO> inventoryLineItemDTOs =
                inventoryService.findInventoryLineItemsByCorrelationId(correlationId);

        List<ProductInventoryLineItemDTO> updatedInventories = new ArrayList<>();

        inventoryLineItemDTOs.forEach(currItem -> updatedInventories.add(currItem.toBuilder().isReserved(false).build()));

        inventoryService.saveInventories(updatedInventories);
    }

    private void reserveInventoryAtomically(final ReserveInventoryRequestDTO inventoryRequestDTO) {
        Lock inventoryLock = LockFactory.getLock(inventoryRequestDTO.getSkuCode());
        try {
            inventoryLock.tryLock(5, TimeUnit.SECONDS);
            inventoryService.reserveInventory(inventoryRequestDTO);
        } catch (InterruptedException e) {
            throw new ServerConflictException(e.getMessage(), e);
        } finally {
            inventoryLock.unlock();
        }
    }
}
