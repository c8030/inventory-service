package io.codex.service.inventoryservice.business.exception;

import io.codex.service.core.exceptions.base.NotFoundException;

public class InventoryNotFoundException extends NotFoundException {

    public InventoryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public InventoryNotFoundException(String message) {
        super(message);
    }
}
