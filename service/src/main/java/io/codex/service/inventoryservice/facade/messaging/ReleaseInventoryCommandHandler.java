package io.codex.service.inventoryservice.facade.messaging;

import io.codex.service.inventoryservice.business.manager.InventoryManager;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.camunda.bpm.client.spring.annotation.ExternalTaskSubscription;
import org.camunda.bpm.client.task.ExternalTask;
import org.camunda.bpm.client.task.ExternalTaskHandler;
import org.camunda.bpm.client.task.ExternalTaskService;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@RequiredArgsConstructor
@ExternalTaskSubscription("COMMAND.RELEASE_INVENTORY")
public class ReleaseInventoryCommandHandler implements ExternalTaskHandler {

    private final InventoryManager inventoryManager;

    @Override
    public void execute(ExternalTask externalTask, ExternalTaskService externalTaskService) {
        log.info("executing release inventory command");

        inventoryManager.releaseInventory(externalTask.getBusinessKey());

        externalTaskService.complete(externalTask);
    }

}
