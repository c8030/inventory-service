package io.codex.service.inventoryservice.business.service.impl;

import io.codex.service.inventoryservice.business.domain.model.ProductInventoryLineItem;
import io.codex.service.inventoryservice.business.exception.InventoryNotFoundException;
import io.codex.service.inventoryservice.business.mapper.ProductInventoryLineItemDTOMapper;
import io.codex.service.inventoryservice.business.repository.ProductInventoryLineItemRepository;
import io.codex.service.inventoryservice.business.service.InventoryService;
import io.codex.service.inventoryservice.dto.ProductInventoryLineItemDTO;
import io.codex.service.inventoryservice.dto.request.ReserveInventoryRequestDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@RequiredArgsConstructor
public class InventoryServiceImpl implements InventoryService {

    private final ProductInventoryLineItemRepository inventoryLineItemRepository;

    private final ProductInventoryLineItemDTOMapper inventoryLineItemDTOMapper;

    @Override
    @Transactional
    public void reserveInventory(final ReserveInventoryRequestDTO reserveInventoryRequestDTO) {
        for(int i=0; i< reserveInventoryRequestDTO.getQuantity(); i++){
            reserveLineItemForSKU(reserveInventoryRequestDTO.getSkuCode());
        }
    }

    private void reserveLineItemForSKU(final String skuCode) {
        ProductInventoryLineItem productInventoryLineItem =
                inventoryLineItemRepository.findFirstBySkuCodeAndIsReservedFalse(skuCode)
                        .orElseThrow(() -> new InventoryNotFoundException(String.format("Inventory not found for sku code: %S", skuCode)));
        productInventoryLineItem.setIsReserved(true);

        inventoryLineItemRepository.save(productInventoryLineItem);
    }

    @Override
    public List<ProductInventoryLineItemDTO> findInventoryLineItemsByCorrelationId(final String correlationId) {
        return inventoryLineItemDTOMapper.toDTOs(inventoryLineItemRepository.findByCorrelationId(correlationId));
    }

    @Override
    public void saveInventories(List<ProductInventoryLineItemDTO> inventoryLineItemDTOS) {
        inventoryLineItemRepository.saveAll(inventoryLineItemDTOMapper.toEntities(inventoryLineItemDTOS));
    }
}
