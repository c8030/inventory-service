package io.codex.service.inventoryservice.dto;

import io.codex.service.dto.core.BaseDTO;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ProductInventoryLineItemDTO extends BaseDTO {

    private Long id;

    private String skuCode;

    private String skuIdentifier;
    private Boolean isReserved;

    @Builder(toBuilder = true)
    public ProductInventoryLineItemDTO(LocalDateTime createdDateTime, LocalDateTime updatedDateTime, Long id, String skuCode, String skuIdentifier, Boolean isReserved) {
        super(createdDateTime, updatedDateTime);
        this.id = id;
        this.skuCode = skuCode;
        this.skuIdentifier = skuIdentifier;
        this.isReserved = isReserved;
    }
}
