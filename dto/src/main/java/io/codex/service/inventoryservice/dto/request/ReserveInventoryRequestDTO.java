package io.codex.service.inventoryservice.dto.request;

import lombok.*;

@Value
public class ReserveInventoryRequestDTO {

    private String saleChannel;

    private String skuCode;

    private Integer quantity;

    @Builder
    public ReserveInventoryRequestDTO(String saleChannel, String skuCode, Integer quantity) {
        this.saleChannel = saleChannel;
        this.skuCode = skuCode;
        this.quantity = quantity;
    }
}
